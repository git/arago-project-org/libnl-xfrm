#
#   Libnl XFRM Makefile
#
#  NOTE:
#      (C) Copyright 2012 Texas Instruments, Inc.
# 

# Root Dir
ROOTDIR = ../../..

# Build environment:
ARCH?=arm
CROSS_COMPILE?=arm-none-linux-gnueabi-

# Tool Definitions:
CC?=$(CROSS_COMPILE)gcc
AR?=$(CROSS_COMPILE)ar
LD?=$(CROSS_COMPILE)ld

# Libnl specific paths
LIBNL_INC_DIR ?= /usr/local/include
LIBNL_LIB_DIR ?= /usr/local/lib

# Library paths
LIBS = 

# Include paths
INCLUDES = 

# Linker and compiler flags
STDINCL = -I$(ROOTDIR) -I$(LIBNL_INC_DIR)
LOCINCL = -g -Wall -D_LITTLE_ENDIAN -D_GNU_SOURCE -I. -I./include 
CFLAGS ?= $(STDINCL)
CFLAGS += $(LOCINCL)

STDLIB = -L$(LIBNL_LIB_DIR)  
LOCLIB = -g -lnl-3 -lnl-route-3 -rdynamic -lresolv -ldl 
LDFLAGS ?= $(STDLIB)
LDFLAGS += $(LOCLIB)
 
AR_OPTS += rcs

#Libnl XFRM library
LIBNL_XFRM_LIB_OBJS = src/ae.o src/selector.o src/lifetime.o src/template.o src/sp.o src/sa.o
LIBNL_XFRM_LIB = lib/libnl-xfrm

#Libnl XFRM test
LIBNL_XFRM_TEST_OBJS = test/main.o
LIBNL_XFRM_TEST_OUT = test/libnl-xfrm-test.out

all: $(LIBNL_XFRM_LIB).so $(LIBNL_XFRM_LIB).a $(LIBNL_XFRM_TEST_OUT)

.PHONY: all build clean distclean

$(LIBNL_XFRM_TEST_OUT): $(LIBNL_XFRM_TEST_OBJS) $(LIBNL_XFRM_LIB).a
		$(CC) $(INCLUDES) $(CFLAGS) ${LDFLAGS} ${LIBS} -o $@ $^ ./$(LIBNL_XFRM_LIB).a

$(LIBNL_XFRM_LIB).so:  $(LIBNL_XFRM_LIB_OBJS)
		$(CC) ${LDFLAGS} -shared -o $@ $^

$(LIBNL_XFRM_LIB).a:  $(LIBNL_XFRM_LIB_OBJS)
		$(AR) ${ARFLAGS} $@ $^

%.o: %.c
		$(CC) $(INCLUDES) $(CFLAGS) -c -o $@ $<

clean:
		rm -rf $(LIBNL_XFRM_LIB).so $(LIBNL_XFRM_LIB).a $(LIBNL_XFRM_TEST_OUT) src/*.o test/*.o

distclean: clean
