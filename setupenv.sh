#!/bin/sh
# Source this script before starting the build for NETFP Proxy

# Cross tool prefix
export CC=${CROSS_COMPILE}gcc
export AR=${CROSS_COMPILE}ar
export LD=${CROSS_COMPILE}ld

# Libnl include directory
export LIBNL_INC_DIR=$ARAGODIR/include

# Libnl lib directory
export LIBNL_LIB_DIR=$ARAGODIR/lib
