/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
/* Libnl includes */
#include <netlink/addr.h>
#include <netlink/cli/utils.h>
#include <netlink/xfrm/ae.h>
#include <netlink/xfrm/sa.h>
#include <netlink/xfrm/sp.h>
#include <netlink/xfrm/selector.h>
#include <netlink/xfrm/lifetime.h>

/* Standard library includes */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* Local definitions */
#define PROTO_ESP       50
#define XFRM_INF        (~(__u64)0)

/* Data structure to hold all the Libnl XFRM info */
typedef struct xfrm_netlink_ctx_t 
{
    struct nl_cache_mngr*   cache_mngr;  
    struct nl_cache*        sa_cache;
    struct nl_cache*        sp_cache;
    struct nl_sock*         nl_sock;        
} xfrm_netlink_ctx_t;

/* Libnl XFRM info object */
xfrm_netlink_ctx_t  xfrm_mcb;

/**
 *  @b Description
 *  @n  
 *      SA event handler
 *
 *  @param[in]  cache 
 *      Corresponding SA cache handle
 *  @param[in]  obj
 *      SA Object that got updated/created/removed as a result 
 *      of the event
 *  @param[in]  action
 *      Event that occured
 *  @param[in]  data 
 *      Any user specific data that was passed during event
 *      registration is passed back here.
 *
 *  @retval
 *      Not Applicable.
 */
static void xfrm_receive_sa_updates (struct nl_cache* cache, struct nl_object* obj, int action, void* data)
{
    static struct nl_dump_params    dump_params;
    dump_params.dp_type = NL_DUMP_LINE;
    dump_params.dp_fd = stdout;

    printf ("DEBUG: Received SA update ... action: %d\n",action);

    //nl_cache_dump (xfrm_mcb.sa_cache, &dump_params);
    if (xfrmnl_sa_is_expiry_reached ((struct xfrmnl_sa*) obj))
    {
        if (xfrmnl_sa_is_hardexpiry_reached ((struct xfrmnl_sa*)obj))            
            printf ("DEBUG: Got SA Hard Lifetime Expiry event!\n");
        else        
            printf ("DEBUG: Got SA Expiry event!\n");
    }
    nl_object_dump(obj, &dump_params);
}

/**
 *  @b Description
 *  @n  
 *      SP event handler
 *
 *  @param[in]  cache 
 *      Corresponding SP cache handle
 *  @param[in]  obj
 *      SP Object that got updated/created/removed as a result 
 *      of the event
 *  @param[in]  action
 *      Event that occured
 *  @param[in]  data 
 *      Any user specific data that was passed during event
 *      registration is passed back here.
 *
 *  @retval
 *      Not Applicable.
 */
static void xfrm_receive_sp_updates (struct nl_cache* cache, struct nl_object* obj, int action, void* data)
{
    static struct nl_dump_params    dump_params;
    dump_params.dp_type = NL_DUMP_LINE;
    dump_params.dp_fd = stdout;

    printf ("DEBUG: Received SP update ... action: %d\n",action);

    //nl_cache_dump (xfrm_mcb.sp_cache, &dump_params);
    nl_object_dump(obj, &dump_params);
}

/**
 *  @b Description
 *  @n  
 *      Polls for any XFRM updates from kernel using Libnl/Libnl-XFRM 
 *      APIs
 *
 *  @param[in]  timeout_val_ms
 *      Maximum time (milliseconds) to wait for any event from kernel
 *
 *  @retval
 *      Not Applicable.
 */
void xfrm_poll_updates (uint32_t timeout_val_ms)
{
    int32_t         num_events;

    /* Wait for any event notifications from the cache manager for the timeout 
     * value (in milliseconds) provided. */
    if ((num_events = nl_cache_mngr_poll (xfrm_mcb.cache_mngr, timeout_val_ms)) < 0)
    {
        /* Timeout reached. No updates to report */            
    }
    else
    {
        /* Timeout reached. Found event notifications. */            
    }

    return;
}

/**
 *  @b Description
 *  @n  
 *      Cleans up Libnl XFRM cache and all relevant info
 *
 *  @retval
 *      Not Applicable.
 */
void xfrm_netlink_exit (void)
{
    /* Disconnect and free the XFRM netlink socket */        
    nl_close (xfrm_mcb.nl_sock);
    nl_socket_free (xfrm_mcb.nl_sock);        
    return;
}

/**
 *  @b Description
 *  @n  
 *      Initializes Libnl for XFRM and sets up all relevant
 *      data structures.
 *
 *  @retval
 *      Not Applicable.
 */
int32_t xfrm_netlink_init (void)
{
    int32_t     retval;

    /* Initialize XFRM netlink MCB */
    memset ((void *)&xfrm_mcb, 0, sizeof (xfrm_netlink_ctx_t));

    /* Setup a netlink socket that we can use for sending AE events to kernel */
	if ((xfrm_mcb.nl_sock = nl_socket_alloc()) == NULL)
    {
        printf ("Unable to allocate netlink socket, error: %d", errno);
        return -1;
    }

    /* Connect this socket to kernel's XFRM subsystem */
	if ((retval = nl_connect (xfrm_mcb.nl_sock, NETLINK_XFRM)) < 0)
    {
        printf ("Unable to connect netlink socket: %s", nl_geterror (retval));
        return -1;
    }

    /* Next, create a Cache manager for NETLINK_XFRM so that we receive XFRM events from kernel */
    if ((retval = nl_cache_mngr_alloc (NULL, NETLINK_XFRM, NL_AUTO_PROVIDE, &xfrm_mcb.cache_mngr)) < 0)
    {
        printf ("Error allocating NETLINK_XFRM Cache manager \n");            
        xfrm_netlink_exit ();
        return -1;
    }

    /* Add caches for all the XFRM components we want to monitor */
    if ((retval = nl_cache_mngr_add (xfrm_mcb.cache_mngr, "xfrm/sa", 
                                     &xfrm_receive_sa_updates, NULL, &xfrm_mcb.sa_cache)) < 0)
    {
        printf ("Error setting up XFRM SA cache, error: %d \n", retval);
        nl_cache_mngr_free (xfrm_mcb.cache_mngr);
        xfrm_netlink_exit ();
        return -1;
    }
    if ((retval = nl_cache_mngr_add (xfrm_mcb.cache_mngr, "xfrm/sp", 
                                     &xfrm_receive_sp_updates, NULL, &xfrm_mcb.sp_cache)) < 0)
    {
        printf ("Error setting up XFRM SP cache, error: %d \n", retval);
        nl_cache_mngr_free (xfrm_mcb.cache_mngr);
        xfrm_netlink_exit ();
        return -1;
    }

    /* XFRM netlink init done. Return success. */
    return 0;
}

/**
 *  @b Description
 *  @n  
 *     Utility function to convert a given string to 32 bit 
 *     integer value.
 *
 *  @param[in]  arg
 *      String to be converted
 *
 *  @retval
 *      <0 - error converting
 *      >0 - converted 32 bit integer value
 */
int32_t parse_u32(const char *arg)
{
	unsigned long lval;
	char *endptr;

	lval = strtoul(arg, &endptr, 0);
	if (endptr == arg || lval == ULONG_MAX)
    {
		printf("Unable to parse \"%s\", not a number.", arg);
        return -1;
    }

	return (uint32_t) lval;
}

/**
 *  @b Description
 *  @n  
 *     Utility function to convert a given string to 64 bit 
 *     integer value.
 *
 *  @param[in]  arg
 *      String to be converted
 *
 *  @retval
 *      <0 - error converting
 *      >0 - converted 64 bit integer value
 */
int64_t parse_u64(const char *arg)
{
	unsigned long long llval;
	char *endptr;

	llval = strtoull(arg, &endptr, 0);
	if (endptr == arg || llval == ULLONG_MAX)
    {
		printf("Unable to parse \"%s\", not a number.", arg);
        return -1;
    }

	return (uint64_t) llval;
}

/**
 *  @b Description
 *  @n  
 *     Tests basic Libnl-XFRM AE APIs.
 *
 *  @param[in]  argc
 *      Number of command line arguments passed to this 
 *      program
 *
 *  @param[in]  argv
 *      Command line arguments passed to the program
 *
 *  @retval
 *      Not applicable
 */
void run_ae_test (int argc, char** argv)
{
    struct xfrmnl_ae        *ae, *ae_new;
    struct nl_addr          *addr;
    uint32_t                spi, mark_m = 0, mark_v = 0;
    uint64_t                bytes, packets, addtime, usetime;
    int32_t                 retval;
	struct nl_dump_params   params = {
		.dp_fd = stdout,
		.dp_type = NL_DUMP_DETAILS,
	};
    char tmpbuf [256];

    /* Get input from command line and validate it */
    if (argc < 8)
    {
        printf ("libnl-xfrm-test.out ae <addr> <spi> <cur_bytes> <cur_packets> <cur_addtime> <cur_usetime> \n");          
        return;
    }

    /* Get the SA destination address */
    if (nl_addr_parse (argv[2], AF_INET, &addr) < 0)
    {
        printf ("libnl-xfrm-test.out ae <addr> <spi> <cur_bytes> <cur_packets> <cur_addtime> <cur_usetime> \n");          
        return;
    }

    /* Get the SA SPI */
    spi = parse_u32(argv[3]);

    /* Get the SA current lifetime to be propagated to the kernel */
    bytes   = parse_u64(argv[4]);
    packets = parse_u64(argv[5]);
    addtime = parse_u64(argv[6]);
    usetime = parse_u64(argv[7]);

    /* Get the current SA stats and info from the kernel and dump it */
    if ((retval = xfrmnl_ae_get_kernel (xfrm_mcb.nl_sock, addr, spi, PROTO_ESP, mark_m, mark_v, &ae)) < 0)
	{
        printf ("Error getting AE for SPI: 0x%x addr: %s protocol: %d \n",
                spi, nl_addr2str (addr, tmpbuf, 256), PROTO_ESP);
        return;
    }
    nl_object_dump ((struct nl_object*) ae, &params);            

    /* Create a new XFRM AE object and set it up with the new SA lifetime info */
    ae_new = (struct xfrmnl_ae*) nl_object_clone ((struct nl_object*)ae);
    xfrmnl_ae_set_curlifetime (ae_new, bytes, packets, addtime, usetime);

    /* Proagate the new lifetime to the kernel */
    xfrmnl_ae_set (xfrm_mcb.nl_sock, ae_new, 0);

    /* Free the old, new XFRM AE objects */
    xfrmnl_ae_put (ae_new);
    xfrmnl_ae_put (ae);

    /* Get the latest current SA stats and info from kernel and dump it */
    if ((retval = xfrmnl_ae_get_kernel (xfrm_mcb.nl_sock, addr, spi, 50, 0, 0, &ae)) < 0)
	{
        printf ("Error getting AE for SPI: 0x%x addr: %s protocol: %d \n",
                spi, nl_addr2str (addr, tmpbuf, 256), PROTO_ESP);
        return;
    }
    nl_object_dump ((struct nl_object*) ae, &params);            
    xfrmnl_ae_put (ae);

    return;
}

/**
 *  @b Description
 *  @n  
 *     Tests various Libnl-XFRM SP get() APIs and dumps
 *     all relevant SP info
 *
 *  @param[in]  sp
 *      SP object on which test needs to be performed
 *
 *  @retval
 *      Not applicable
 */
void test_sp_get (struct xfrmnl_sp* sp)
{
    char                dir[32], action[32], share[32], flags[32];
	char                dst[INET6_ADDRSTRLEN+5], src[INET6_ADDRSTRLEN+5];
    time_t              add_time, use_time;
    struct tm           *add_time_tm, *use_time_tm;
    struct xfrmnl_sel   *sel;
    struct xfrmnl_ltime_cfg *ltime;
    uint64_t            bytes, packets;
    uint32_t            mark_m, mark_v; 

    sel = xfrmnl_sp_get_sel (sp);
    nl_addr2str(xfrmnl_sel_get_saddr (sel), src, sizeof(src));
    nl_addr2str (xfrmnl_sel_get_daddr (sel), dst, sizeof (dst));
    nl_af2str (xfrmnl_sel_get_family (sel), dir, 32);
	printf("src %s dst %s family: %s\n", src, dst, dir);
    printf("src port/mask: %d/%d dst port/mask: %d/%d\n", 
                  xfrmnl_sel_get_dport (sel), xfrmnl_sel_get_dportmask (sel), 
                  xfrmnl_sel_get_sport (sel), xfrmnl_sel_get_sportmask (sel));
    printf("protocol: %d ifindex: %u uid: %u\n", 
                  xfrmnl_sel_get_proto (sel), xfrmnl_sel_get_ifindex (sel), 
                  xfrmnl_sel_get_userid (sel));

    xfrmnl_sp_dir2str (xfrmnl_sp_get_dir(sp), dir, 32);
    xfrmnl_sp_action2str (xfrmnl_sp_get_action(sp), action, 32);
    xfrmnl_sp_share2str (xfrmnl_sp_get_share(sp), share, 32);
    xfrmnl_sp_flags2str (xfrmnl_sp_get_flags(sp), flags, 32);
	printf("\tdir: %s action: %s index: %u priority: %u share: %s flags: %s(0x%x) \n",
            dir, action, xfrmnl_sp_get_index(sp), xfrmnl_sp_get_priority(sp), share, flags, 
            xfrmnl_sp_get_flags(sp));

    ltime = xfrmnl_sp_get_lifetime_cfg (sp); 
	printf("\tlifetime configuration: \n");
    if (xfrmnl_ltime_cfg_get_soft_bytelimit (ltime) == XFRM_INF)
        sprintf (dir, "INF");            
    else
        sprintf (dir, "%llu", xfrmnl_ltime_cfg_get_soft_bytelimit (ltime));            
    if (xfrmnl_ltime_cfg_get_soft_packetlimit (ltime) == XFRM_INF)
        sprintf (action, "INF");            
    else
        sprintf (action, "%llu", xfrmnl_ltime_cfg_get_soft_packetlimit (ltime));            
	printf("\t\tsoft limit: %s (bytes), %s (packets)\n", dir, action);
    if (xfrmnl_ltime_cfg_get_hard_bytelimit (ltime) == XFRM_INF)
        sprintf (flags, "INF");            
    else
        sprintf (flags, "%llu", xfrmnl_ltime_cfg_get_hard_bytelimit (ltime));            
    if (xfrmnl_ltime_cfg_get_hard_packetlimit (ltime) == XFRM_INF)
        sprintf (share, "INF");            
    else
        sprintf (share, "%llu", xfrmnl_ltime_cfg_get_hard_packetlimit (ltime));            
	printf("\t\thard limit: %s (bytes), %s (packets)\n", flags, share);
	printf("\t\tsoft add_time: %llu (seconds), soft use_time: %llu (seconds) \n", xfrmnl_ltime_cfg_get_soft_addexpires (ltime), xfrmnl_ltime_cfg_get_soft_useexpires (ltime)); 
	printf("\t\thard add_time: %llu (seconds), hard use_time: %llu (seconds) \n", xfrmnl_ltime_cfg_get_hard_addexpires (ltime), xfrmnl_ltime_cfg_get_hard_useexpires (ltime)); 

    xfrmnl_sp_get_curlifetime (sp, &bytes, &packets, (unsigned long long*)&add_time, (unsigned long long*)&use_time);
	printf("\tlifetime current: \n");
	printf("\t\t%llu bytes, %llu packets\n", bytes, packets);

    if (add_time != 0)
    {
        add_time_tm = gmtime (&add_time);
        strftime (dst, INET6_ADDRSTRLEN+5, "%Y-%m-%d %H-%M-%S", add_time_tm);
    }
    else
    {
        sprintf (dst, "%s", "-");            
    }

    if (use_time != 0)
    {
        use_time_tm = gmtime (&use_time);
        strftime (src, INET6_ADDRSTRLEN+5, "%Y-%m-%d %H-%M-%S", use_time_tm);
    }
    else
    {
        sprintf (src, "%s", "-");            
    }
	printf("\t\tadd_time: %s, use_time: %s\n", dst, src); 

    if (xfrmnl_sp_get_mark (sp, &mark_m, &mark_v) == 0)
	    printf("\tMark mask: 0x%x Mark value: 0x%x\n", mark_m, mark_v);

    return;
}

/**
 *  @b Description
 *  @n  
 *     Tests update/delete/set Libnl-XFRM SP APIs.
 *
 *  @param[in]  sp
 *      SP object on which test needs to be performed
 *
 *  @retval
 *      Not applicable
 */
void test_sp_set (struct xfrmnl_sp* sp)
{
    struct xfrmnl_sp        *sp_new;        
	struct nl_dump_params   params = {
		.dp_fd = stdout,
		.dp_type = NL_DUMP_DETAILS,
	};
    uint32_t mark1_m, mark1_v, mark2_m, mark2_v;
    char dir[32];

    printf ("Testing SP update... \n");
    /* Setup a mark and update it in the kernel */
    xfrmnl_sp_set_mark (sp, 0x07, 0xffffffff);
    xfrmnl_sp_update (xfrm_mcb.nl_sock, sp, 0);
    nl_object_dump ((struct nl_object *)sp, &params);

    printf ("Testing SP get from kernel ... \n");
    /* Get the SP info from the kernel and verify if mark infact got set */
    if (xfrmnl_sp_get_kernel (xfrm_mcb.nl_sock, 
                          xfrmnl_sp_get_index (sp), 
                          xfrmnl_sp_get_dir (sp),
                          0x7,
                          0xffffffff,
                          &sp_new) < 0)
    {
        printf ("Error getting SP for index: %d dir: %s \n",
                xfrmnl_sp_get_index (sp), xfrmnl_sp_dir2str (xfrmnl_sp_get_dir(sp), dir, 32));
        return;
    }
    xfrmnl_sp_get_mark (sp, &mark1_m, &mark1_v);
    xfrmnl_sp_get_mark (sp_new, &mark2_m, &mark2_v);
    if ((mark1_m & mark1_v) != (mark2_m & mark2_v))
    {
        printf ("SP mark update failed set to: %d got %dn",  
                mark1_m & mark1_v,
                mark2_m & mark2_v);
        nl_object_dump ((struct nl_object *)sp_new, &params);
    }
    else
    {
        printf ("SP Mark updated succesfully \n");           
        nl_object_dump ((struct nl_object *)sp_new, &params);
    }
    xfrmnl_sp_put (sp_new);
           
    printf ("Testing SP delete... \n");
    /* Test delete SP */
    xfrmnl_sp_delete (xfrm_mcb.nl_sock, sp, 0);
    nl_cache_dump (xfrm_mcb.sp_cache, &params);
    printf ("SP deleted succesfully \n");           

    return;
}

/**
 *  @b Description
 *  @n  
 *     Tests basic Libnl-XFRM SP APIs.
 *
 *  @param[in]  argc
 *      Number of command line arguments passed to this 
 *      program
 *
 *  @param[in]  argv
 *      Command line arguments passed to the program
 *
 *  @retval
 *      Not applicable
 */
void run_sp_test (int argc, char** argv)
{
    struct xfrmnl_sp        *sp;
    uint32_t                dir, index;
	struct nl_dump_params   params = {
		.dp_fd = stdout,
		.dp_type = NL_DUMP_DETAILS,
	};

    if (argc < 4)
    {
        printf ("libnl-xfrm-test.out sp <dir> <sp_index>\n");          
        return;
    }

    dir = xfrmnl_sp_str2dir(argv[2]);
    index = parse_u32(argv[3]);

    /* Get the SP from the kernel and dump it */
    if ((sp = xfrmnl_sp_get (xfrm_mcb.sp_cache, index, dir)) == NULL)
	{
        printf ("Error getting SP for index: %d dir: %d \n", index, dir);
        nl_cache_dump (xfrm_mcb.sp_cache, &params);
        return;
    }
    nl_object_dump ((struct nl_object*) sp, &params);            

    printf ("Testing SP get() APIs ... \n");
    test_sp_get (sp);

    printf ("Testing SP set()/delete() APIs ... \n");
    test_sp_set (sp);

    /* Free the SP object */
    xfrmnl_sp_put (sp);

    return;
}

/**
 *  @b Description
 *  @n  
 *     Tests various Libnl-XFRM SA get() APIs and dumps
 *     all relevant SA info
 *
 *  @param[in]  sa
 *      SA object on which test needs to be performed
 *
 *  @retval
 *      Not applicable
 */
void test_sa_get (struct xfrmnl_sa* sa)
{
	char                dst[INET6_ADDRSTRLEN+5], src[INET6_ADDRSTRLEN+5];
    char                flags[128], mode[128];
    time_t              add_time, use_time;
    struct tm           *add_time_tm, *use_time_tm;
    struct xfrmnl_ltime_cfg *ltime;
    uint64_t            bytes, packets;
    uint32_t            mark_m, mark_v; 
	struct nl_dump_params   params = {
		.dp_fd = stdout,
		.dp_type = NL_DUMP_DETAILS,
	};

    printf("src %s dst %s family: %s\n", nl_addr2str(xfrmnl_sa_get_saddr (sa), src, sizeof(src)), 
                nl_addr2str(xfrmnl_sa_get_daddr (sa), dst, sizeof(dst)), 
                nl_af2str (xfrmnl_sa_get_family (sa), flags, sizeof (flags)));

	printf("\tproto %d spi 0x%x reqid %u\n", xfrmnl_sa_get_proto (sa), 
                xfrmnl_sa_get_spi (sa), xfrmnl_sa_get_reqid (sa));

    xfrmnl_sa_flags2str(xfrmnl_sa_get_flags (sa), flags, sizeof (flags));
    xfrmnl_sa_mode2str(xfrmnl_sa_get_mode (sa), mode, sizeof (mode));
	printf("\tmode: %s flags: %s (0x%x) seq: %u replay window: %u\n", mode, flags, 
                xfrmnl_sa_get_flags (sa), xfrmnl_sa_get_seq (sa), xfrmnl_sa_get_replay_window (sa)); 

    ltime = xfrmnl_sa_get_lifetime_cfg (sa);
	printf("\tlifetime configuration: \n");
    if (xfrmnl_ltime_cfg_get_soft_bytelimit (ltime) == XFRM_INF)
        sprintf (flags, "INF");            
    else
        sprintf (flags, "%llu", xfrmnl_ltime_cfg_get_soft_bytelimit (ltime));            
    if (xfrmnl_ltime_cfg_get_soft_packetlimit (ltime) == XFRM_INF)
        sprintf (mode, "INF");            
    else
        sprintf (mode, "%llu", xfrmnl_ltime_cfg_get_soft_packetlimit (ltime));            
	printf("\t\tsoft limit: %s (bytes), %s (packets)\n", flags, mode);
    if (xfrmnl_ltime_cfg_get_hard_bytelimit (ltime) == XFRM_INF)
        sprintf (flags, "INF");            
    else
        sprintf (flags, "%llu", xfrmnl_ltime_cfg_get_hard_bytelimit (ltime));            
    if (xfrmnl_ltime_cfg_get_hard_packetlimit (ltime) == XFRM_INF)
        sprintf (mode, "INF");            
    else
        sprintf (mode, "%llu", xfrmnl_ltime_cfg_get_hard_packetlimit (ltime));            
	printf("\t\thard limit: %s (bytes), %s (packets)\n", flags, mode);
	printf("\t\tsoft add_time: %llu (seconds), soft use_time: %llu (seconds) \n", xfrmnl_ltime_cfg_get_soft_addexpires (ltime), xfrmnl_ltime_cfg_get_soft_useexpires (ltime)); 
	printf("\t\thard add_time: %llu (seconds), hard use_time: %llu (seconds) \n", xfrmnl_ltime_cfg_get_hard_addexpires (ltime), xfrmnl_ltime_cfg_get_hard_useexpires (ltime)); 

    xfrmnl_sa_get_curlifetime (sa, &bytes, &packets, (unsigned long long*)&add_time, (unsigned long long*)&use_time);
	printf("\tlifetime current: \n");
	printf("\t\t%llu bytes, %llu packets\n", bytes, packets);
    if (add_time != 0)
    {
        add_time_tm = gmtime (&add_time);
        strftime (flags, 128, "%Y-%m-%d %H-%M-%S", add_time_tm);
    }
    else
    {
        sprintf (flags, "%s", "-");            
    }

    if (use_time != 0)
    {
        use_time_tm = gmtime (&use_time);
        strftime (mode, 128, "%Y-%m-%d %H-%M-%S", use_time_tm);
    }
    else
    {
        sprintf (mode, "%s", "-");            
    }
	printf("\t\tadd_time: %s, use_time: %s\n", flags, mode); 

    if (xfrmnl_sa_get_mark (sa, &mark_m, &mark_v) == 0)
	    printf("\tMark mask: %u Mark value: %u\n", mark_m, mark_v);

	printf("\tselector info: \n");
    xfrmnl_sel_dump (xfrmnl_sa_get_sel (sa), &params);

    return;
}

/**
 *  @b Description
 *  @n  
 *     Tests update/delete/set Libnl-XFRM SA APIs.
 *
 *  @param[in]  sa
 *      SA object on which test needs to be performed
 *
 *  @retval
 *      Not applicable
 */
void test_sa_set (struct xfrmnl_sa* sa)
{
    struct xfrmnl_sa    *sa_new;        
	struct nl_dump_params   params = {
		.dp_fd = stdout,
		.dp_type = NL_DUMP_DETAILS,
	};
    char tmpbuf[256];
    struct xfrmnl_ltime_cfg* ltime;
    uint64_t    expires_old = 161859, expires_new;

    printf ("Testing SA update ... \n");
    /* Change replay window and update it in the kernel */
    ltime = xfrmnl_sa_get_lifetime_cfg (sa);
    xfrmnl_ltime_cfg_set_soft_addexpires (ltime, expires_old);
    xfrmnl_sa_update (xfrm_mcb.nl_sock, sa, 0);
    nl_object_dump ((struct nl_object *)sa, &params);

    printf ("Testing SA get from kernel ... \n");
    /* Get the SA info from the kernel and verify if replay window
     * in fact got changed */
    if (xfrmnl_sa_get_kernel (xfrm_mcb.nl_sock, 
                          xfrmnl_sa_get_daddr (sa), 
                          xfrmnl_sa_get_spi (sa),
                          xfrmnl_sa_get_proto (sa),
                          0x0, 0x0,
                          &sa_new) < 0)
    {
        printf ("Error getting SA for SPI: 0x%x addr: %s protocol: %d \n",
                xfrmnl_sa_get_spi (sa), nl_addr2str (xfrmnl_sa_get_daddr (sa), tmpbuf, 256), 
                xfrmnl_sa_get_proto (sa));
        return;
    }
    ltime = xfrmnl_sa_get_lifetime_cfg (sa_new);
    expires_new = xfrmnl_ltime_cfg_get_soft_addexpires (ltime);
    if (expires_new != expires_old)
    {
        printf ("SA lifetime config update failed set to: %llu got %llu\n",  
                expires_old, expires_new);
        nl_object_dump ((struct nl_object *)sa_new, &params);
    }
    else
    {
        printf ("SA lifetime config updated succesfully \n");           
        nl_object_dump ((struct nl_object *)sa_new, &params);
    }
    xfrmnl_sa_put (sa_new);

    printf ("Testing SA delete ... \n");
    /* Test delete SA */
    xfrmnl_sa_delete (xfrm_mcb.nl_sock, sa, 0);
    nl_cache_dump (xfrm_mcb.sa_cache, &params);

    return;
}

/**
 *  @b Description
 *  @n  
 *     Tests basic Libnl-XFRM SA APIs.
 *
 *  @param[in]  argc
 *      Number of command line arguments passed to this 
 *      program
 *
 *  @param[in]  argv
 *      Command line arguments passed to the program
 *
 *  @retval
 *      Not applicable
 */
void run_sa_test (int argc, char** argv)
{
    struct xfrmnl_sa        *sa;
    struct nl_addr          *addr;
    uint32_t                spi, protocol;
	struct nl_dump_params   params = {
		.dp_fd = stdout,
		.dp_type = NL_DUMP_DETAILS,
	};
    char tmpbuf [256];

    if (argc < 5)
    {
        printf ("libnl-xfrm-test.out sa <addr> <spi> <protocol>\n");          
        return;
    }

    /* Get the SA destination address */
    if (nl_addr_parse (argv[2], AF_INET, &addr) < 0)
    {
        printf ("libnl-xfrm-test.out sa <addr> <spi> <protocol>\n");          
        return;
    }

    /* Get the SA SPI, protocol */
    spi = parse_u32(argv[3]);
    protocol = parse_u32(argv[4]);

    /* Get the SA from the kernel and dump it */
    if ((sa = xfrmnl_sa_get (xfrm_mcb.sa_cache, addr, spi, protocol)) == NULL)
	{
        printf ("Error getting SA for SPI: 0x%x addr: %s protocol: %d \n",
                spi, nl_addr2str (addr, tmpbuf, 256), protocol);
        nl_cache_dump (xfrm_mcb.sa_cache, &params);
        return;
    }
    nl_object_dump ((struct nl_object*) sa, &params);            

    printf ("Testing SA get() APIs ... \n");
    test_sa_get (sa);

    printf ("Testing SA set()/delete() APIs ... \n");
    test_sa_set (sa);

    /* Free the SA object */
    xfrmnl_sa_put (sa);
    
    return;
}

/**
 *  @b Description
 *  @n  
 *     Entry point into the test application.
 *
 *  @param[in]  argc
 *      Number of command line arguments passed to this 
 *      program
 *
 *  @param[in]  argv
 *      Command line arguments passed to the program
 *
 *  @retval
 *      0 - Success
 *      1 - Error
 */
int32_t main (int argc, char** argv)
{
    /* Validate input */
    if (argc < 2)
    {
        printf ("libnl-xfrm-test.out <ae|sp|sa> [params] \n");          
        exit (1);
    }

    /* Setup an XFRM netlink socket */
    if (xfrm_netlink_init () < 0)
    {
        printf ("Error setting up netlink socket \n");
        exit (1);
    }

    if (argv[1][0] == 'a')
    {
        run_ae_test (argc, argv);             
    }
    else if (argv[1][0] == 's' && argv[1][1] == 'p')
    {
        run_sp_test (argc, argv);             
    }
    else if (argv[1][0] == 's' && argv[1][1] == 'a')
    {
        run_sa_test (argc, argv);             
    }
    else
    {
        printf ("libnl-xfrm-test.out <ae|sp|sa> [params] \n");          
        exit (1);
    }

    while (1)
    {
        xfrm_poll_updates (100);
    }

    /* Done. */
    return 0;
}
