/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/ 
#include <xfrm_pvt.h>

char* utils_type2str(int type, char *buf, size_t len, const struct attr_info* attr_map, size_t tbl_len)
{
	size_t i;
	for (i = 0; i < tbl_len; i++) {
		if (tbl[i].i == type) {
			snprintf(buf, len, "%s", tbl[i].a);
			return buf;
		}
	}

	snprintf(buf, len, "0x%x", type);
	return buf;
}

char *__flags2str(int flags, char *buf, size_t len,
		  const struct trans_tbl *tbl, size_t tbl_len)
{
	size_t i;
	int tmp = flags;

	memset(buf, 0, len);
	
	for (i = 0; i < tbl_len; i++) {
		if (tbl[i].i & tmp) {
			tmp &= ~tbl[i].i;
			strncat(buf, tbl[i].a, len - strlen(buf) - 1);
			if ((tmp & flags))
				strncat(buf, ",", len - strlen(buf) - 1);
		}
	}

	return buf;
}

int __str2type(const char *buf, const struct trans_tbl *tbl, size_t tbl_len)
{
	unsigned long l;
	char *end;
	size_t i;

	if (*buf == '\0')
		return -NLE_INVAL;

	for (i = 0; i < tbl_len; i++)
		if (!strcasecmp(tbl[i].a, buf))
			return tbl[i].i;

	l = strtoul(buf, &end, 0);
	if (l == ULONG_MAX || *end != '\0')
		return -NLE_OBJ_NOTFOUND;

	return (int) l;
}
